'use strict'

// Set all const variables
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const uri = 'mongodb://localhost/project';
const port = 3000;
const bodyParser = require("body-parser");
var json;

// Fix deprecation errors
mongoose.set('useUnifiedTopology', true);
mongoose.set('useNewUrlParser', true);

// Connect MongoDB and check connection
mongoose.connect(uri)
const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('Connected to Database')) 

// Start listening to port 3000
app.listen(port, () => console.log(`Server listening on port ${port}`))

// Save runner into MongoDB
function save(obj) {

	var h = Math.floor(Math.random() * 4) + 10; // returns hours from 10 to 13
	var m1 = Math.floor(Math.random() * 6); // returns minutes 0-5
	var m2 = Math.floor(Math.random() * 9); // returns minutes 0-9
	
	var obj2 = {
		name: obj.name,
		lastname: obj.lastname,
		competition_class: obj.competition_class,
		start_time: h + ":" + m1 + m2 // add start_time for runner between 10.00 - 13.00
	};
	
	mongoose.connect(uri, function(err, db) {
		if (err) throw err;
		db.collection("orienteering").insertOne(obj2, function(err, res) {
			if (err) throw err;
			console.log("Runner added into database");
			//db.close();
		});  
	});
}

// Use bodyParser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// REST server to show index.html when user enters localhost:3000
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/front/index.html');
});

// Handle requests coming from the user
app.post('/', (req, res) => {
	save(req.body);
	getJson();
	setTimeout(function(){ // Wait 500ms to get answer from the MongoDB
		res.send(json); // Send current list of runners to the server
		//res.sendStatus(200); // Status = ok = 200
	}, 500)
});

// get runners from the MongoDB (all below (3) functions are related to each other
function getJson() {
	mongoose.connect(uri, connect);
}

function connect(err, db) {
		if (err) throw err;
		//var query = { name: "name" };
		db.collection("orienteering").find().toArray(showResult); // Find all runners
}

function showResult(err, result) {
			if (err) throw err;
			//console.log(result);
			json = result;
}