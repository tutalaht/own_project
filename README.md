# Anytime-course: Software Development Skills: Back-End 2020-21

Install and using instructions for own project.

See short video about project usage YouTube: https://youtu.be/Xvgu5fLElOc

Purpose of this project was to create backend and frontend that handles user requests.
User can add runners into two different classes and after adding user can check starting time
for the competition.

---

## Installation

NOTE: This instruction assumes that you have node.js already installed if not follow links below.

**Windows**: https://phoenixnap.com/kb/install-node-js-npm-on-windows
**Linux**: https://linuxize.com/post/how-to-install-node-js-on-ubuntu-20-04/

After node.js is installed follow below commands to install project.

1. git clone https://bitbucket.org/tutalaht/own_project/src
2. cd src
3. npm install express
4. npm install mongoose

Now project is installed and ready to be used!

---

## Usage

1. node index.js

After that one command project is running and you can enter
to the front page entering **localhost:3000** in the url bar.

---